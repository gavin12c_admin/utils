package com.td.utils;

import org.apache.poi.hssf.usermodel.*;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 提取文件中的中文，并生成Excel
 */
public class WordUtils {
    static List<File> filelist = new ArrayList<>();
    static Set<String> chineseSet = new HashSet<>();

    public static void main(String[] args) {
        try {
            String pathName = "C:\\work\\backend\\PowerServer-manage\\src\\main\\resources\\templates";//文件夹目录
            String toExcelName = "D:\\" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + "系统词汇.xls";//Excel文件名
            setChineseSet(pathName); //添加chineseSet中文单词
            createExcel(chineseSet,toExcelName);//创建Excel
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static void createExcel(Set<String> chineseList,String toExcelName) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("系统词汇");

        int rowNum = 1;
        String[] headers = {"中文词汇", "商务英文"};
        HSSFRow row = sheet.createRow(0);
        //在excel表中添加表头
        for (int i = 0; i < headers.length; i++) {
            HSSFCell cell = row.createCell(i);
            HSSFRichTextString text = new HSSFRichTextString(headers[i]);
            cell.setCellValue(text);
        }
        for (String w : chineseList) {
            HSSFRow row1 = sheet.createRow(rowNum);
            row1.createCell(0).setCellValue(w);
            rowNum++;
        }
        FileOutputStream fileOutputStream = null;
        fileOutputStream = new FileOutputStream(toExcelName);
        workbook.write(fileOutputStream);
        workbook.close();
    }

    public static List<File> getFileList(String strPath) {
        File dir = new File(strPath);
        File[] files = dir.listFiles(); // 该文件目录下文件全部放入数组
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                String fileName = files[i].getName();
                if (files[i].isDirectory()) { // 判断是文件还是文件夹
                    getFileList(files[i].getAbsolutePath()); // 获取文件绝对路径
                } else if (fileName.endsWith("html")) { // 判断文件名是否以结尾
                    String strFileName = files[i].getAbsolutePath();
                    System.out.println("---" + strFileName);
                    filelist.add(files[i]);
                } else {
                    continue;
                }
            }

        }
        return filelist;
    }

    public static void setChineseSet(String pathName) {
        List<File> fileList = getFileList(pathName);
        for (File file:fileList) {
            String s = readFileContent(file);
            filterChineseAdd(s);
        }
    }

    public static String readFileContent(File file) {
        BufferedReader reader = null;
        StringBuffer sbf = new StringBuffer();
        try {
            reader = new BufferedReader(new FileReader(file));
            String tempStr;
            while ((tempStr = reader.readLine()) != null) {
                sbf.append(tempStr);
            }
            reader.close();
            return sbf.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return sbf.toString();
    }

    public static void filterChineseAdd(String cls) {
        StringBuffer word = new StringBuffer();
        for (int i = 0; i < cls.length(); i++) {
            String c = cls.charAt(i) + "";
            if (c.getBytes().length > 1) {
                word.append(c);
                if (i != cls.length() - 1 && (cls.charAt(i + 1) + "").getBytes().length <= 1) {
                    chineseSet.add(word.toString());
                    word.setLength(0);
                }
            }
        }
    }
}
